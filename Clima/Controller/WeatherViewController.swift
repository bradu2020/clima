//
//  ViewController.swift
//  Clima
//
//  Created by Angela Yu on 01/09/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit
import CoreLocation

//allow it to manage the editing and validation of text in textfield
class WeatherViewController: UIViewController {
    
    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    var weatherManager = WeatherManager()
    //create location manager
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //first .delegate = self
        locationManager.delegate = self
        //show popup to request authorization -> add to info.plist property and msg to user
        locationManager.requestWhenInUseAuthorization()
        // .stratUpdatingLocation() //to start monitoring location and report back when it updates
        locationManager.requestLocation()


        //set delegate as current class
        weatherManager.delegate = self
        //textfield will notify WeatherViewController of what is happening
        searchTextField.delegate = self
    }
    
    @IBAction func locationPressed(_ sender: UIButton) {
        //trigger locationManager didUpdateLocation
        //locationManager.requestLocation() but if loc didnt change didUpdate won't be called
        locationManager.requestLocation()


    }
    
}

//extend the class //also create new section to divide the file
//MARK: - UITextFieldDelegate

extension WeatherViewController: UITextFieldDelegate {
    @IBAction func searchPressed(_ sender: UIButton) {
        //dismiss the keyboard
        searchTextField.endEditing(true)
    }
    
    /*The methdos below are triggered by the textfield
     -> multiple textfields can trigger them*/
    //tell viewcontroller that the user pressed return,exec code, return true if textfield should process that return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.endEditing(true)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            return true
        } else
        {
            textField.placeholder = "Type something here"
            return false
        }
    }
    
    //clear text after search (searchPressed and return)
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //optionally unwrap text property to use a string
        if let city = searchTextField.text {
            weatherManager.fetchWeather(cityName: city)
        }
        
        searchTextField.text = ""
    }
}

//MARK: - WeatherManagerDelegate
extension WeatherViewController: WeatherManagerDelegate {
    func didUpdateWeather(_ weatherManager: WeatherManager, weather: WeatherModel) {
        //UILabel.text must be used from main thread only
        //weather is dependant of the completion of the networking session seconds/minutes -> frozen UI
        //temperatureLabel.text = weather.stringTemperature //FIX: wrap in async closure
        DispatchQueue.main.async {
            self.temperatureLabel.text = weather.stringTemperature
            //Create UIImage from string
            self.conditionImageView.image = UIImage(systemName: weather.conditionName)
            
            self.cityLabel.text = weather.cityName
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
}

//MARK: - CLLocationManagerDelegate
extension WeatherViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //locations is an arr of CLLocation //optional binding IF LET in co
        if let location = locations.last {
            //to pause this method once location is fetched, and use  locationManager.requestLocation()
            //when a button is pressed

            locationManager.stopUpdatingLocation()
            let lat = location.coordinate.latitude
            let lon = location.coordinate.longitude
            weatherManager.fetchWeather(latitude: lat, longitude: lon)
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
