//api.openweathermap.org/data/2.5/weather?q={city name}
//4e43e5e62bd179cadecd4597d1ccd90e

import Foundation
import CoreLocation

protocol WeatherManagerDelegate {
    func didUpdateWeather(_ weatherManager: WeatherManager, weather: WeatherModel)
    func didFailWithError(error: Error)
}

struct WeatherManager {
    let weatherURL = "https://api.openweathermap.org/data/2.5/weather?appid=4e43e5e62bd179cadecd4597d1ccd90e&units=metric"

    var delegate: WeatherManagerDelegate?
    
    func fetchWeather(cityName: String) {
        let urlString = "\(weatherURL)&q=\(cityName)"
        performRequest(with: urlString)
    }
    
    func fetchWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let urlString = "\(weatherURL)&lat=\(latitude)&lon=\(longitude)"
        performRequest(with: urlString)
    }
    
    //"hacking" the external parameter" for readability
    func performRequest(with urlString: String) {
        //create url, optionally bind to a url constant (bind if not nil/fail)
        if let url = URL(string: urlString) {
            //create url session
            let session = URLSession(configuration: .default)
            
            //give the session a task
            //completionHandler takes a function as the value
            //trailing closure: execute {} once it completes the dataTask
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
            
                if let safeData = data {
                    //inside a closure, swift does not implicitly add the self. to methods from current class
                    if let weather = self.parseJSON(safeData) {
                        //optional bind the weather object? by using if let
                        
//                        let weatherVC = WeatherViewController()
//                        weatherVC.didUpdateWeather(weather: weather)
                        
                        //DELEGATE PATTERN
                        //inside closure self. to make sure it calls delegate from same class
                        self.delegate?.didUpdateWeather(self, weather: weather) //weatherManager is gonna be self that triggered didUpdateWeather and knows all about weather conditions
                        //if it has a delegate this optional chaining will pass and trigger didUpdateWeather
                        
                    }
                }
            }
            
            
            //start the task !TASKS BEGIN IN SUSPENDED STATE!
            task.resume()
            
        }
    }
    
    func parseJSON(_ weatherData: Data) -> WeatherModel? {
        let decoder = JSONDecoder()

        do {
            //decode also has an output       //.self to pass the data type
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)
//            print(decodedData.name) //from WeatherData.swift struct
//            print(decodedData.main.temp)
//            print(decodedData.weather[0].description)
            
            let id = decodedData.weather[0].id
            let temp = decodedData.main.temp
            let name = decodedData.name
            
            let weather = WeatherModel(conditionId: id, cityName: name, temperature: temp)
//            print(weather.conditionName)
//            print(weather.stringTemperature)
            return weather
            

        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
    
}
    

