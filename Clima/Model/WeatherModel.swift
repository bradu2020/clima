
import Foundation

struct WeatherModel {
    //stored properties
    let conditionId: Int
    let cityName: String
    let temperature: Double

    //computed properties
/*
     var aProperty: Int {
        return 2 + 5
    }
*/
    var stringTemperature: String {
        return String(format: "%.1f",temperature)
    }
    
    var  conditionName: String {
        switch conditionId {
        case 200..<233:
            return "cloud.bolt"
        case 300..<322:
            return "cloud.drizzle"
        case 500..<532:
            return "cloud.rain"
        case 600..<623:
            return "cloud.snow"
        case 701..<782:
            return "cloud.fog"
        case 800:
            return "sun.max"
        case 801...804:
            return "cloud.bolt"
        default:
            return "cloud"
        }
    }
}
